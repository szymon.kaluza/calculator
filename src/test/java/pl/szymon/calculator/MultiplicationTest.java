package pl.szymon.calculator;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class MultiplicationTest {

    private Multiplication objectUnderTest = new Multiplication();

    @Test
    public void shouldCalculateReturnProduct() throws Exception {
        //given
        final int a = 43;
        final int b = 12;
        //when
        int result = objectUnderTest.calculate(a, b);
        //then
        assertThat(result).isEqualTo(516);
    }


}