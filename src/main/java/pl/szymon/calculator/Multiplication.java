package pl.szymon.calculator;

public class Multiplication implements Operator {

    public int calculate(int a, int b) {
        return a * b;
    }
}
