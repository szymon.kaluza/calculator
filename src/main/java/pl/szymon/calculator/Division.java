package pl.szymon.calculator;

public class Division implements Operator {

    public int calculate(int a, int b) {

        return a / b;
    }

    @Override
    public boolean hasValidInput(Integer a, Integer b) {

        return b != 0;
    }
}
