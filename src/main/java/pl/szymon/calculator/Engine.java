package pl.szymon.calculator;

public class Engine {

    public Integer getA() {
        return a;
    }

    public Integer getB() {
        return b;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    private Integer a;
    private Integer b;
    private Operator operator;

    public boolean isOperatorSet() {
        return operator != null;
    }

    private int appendDigit(Integer a, int b) {

        int numberWithAddedDigit;

        if (a == null) {
            numberWithAddedDigit = b;
        } else {
            numberWithAddedDigit = Integer.parseInt(Integer.toString(a) + Integer.toString(b));
        }

        return numberWithAddedDigit;
    }

    public void setNumber(int number) {
        if (!isOperatorSet()) {
            a = appendDigit(a, number);
        } else {
            b = appendDigit(b, number);
        }
    }


    public void setOperator(Operator operator) {
        if (!isOperatorSet()) {
            this.operator = operator;
        }
    }

    public void clearState() {
        this.a = null;
        this.b = null;
        this.operator = null;
    }
    public int evaluate (){
        return operator.calculate(a, b);
    }

    public boolean hasValidInput() {
        return a != null &&
                b != null &&
                operator != null &&
                operator.hasValidInput(a,b);
    }
}
