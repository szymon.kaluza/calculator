package pl.szymon.calculator;

public class Substraction implements Operator {

    public int calculate(int a, int b) {
        return a - b;
    }
}
