package pl.szymon.calculator;

public class OperatorFactory {

    public static Operator getOpButton(char text) {
        if (text == '+') {
            return new Addition();
        } else if (text == '-') {
            return new Substraction();
        } else if (text == '*') {
            return new Multiplication();
        } else if (text == '/') {
            return new Division();
        }
        throw new IllegalArgumentException("Wrong operator" + text);

    }
}
