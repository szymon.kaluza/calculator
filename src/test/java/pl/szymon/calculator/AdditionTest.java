package pl.szymon.calculator;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class AdditionTest {

    private Addition objectUnderTest = new Addition();

    @Test
    public void shouldCalculateReturnSum() throws Exception {
        //given
        final int a = 43;
        final int b = 12;
        //when
        int result = objectUnderTest.calculate(a, b);
        //then
        assertThat(result).isEqualTo(55);
    }

}