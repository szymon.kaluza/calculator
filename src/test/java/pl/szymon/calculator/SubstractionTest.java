package pl.szymon.calculator;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class SubstractionTest {

    private Substraction objectUnderTest = new Substraction();

    @Test
    public void shouldCalculateReturnDifference() throws Exception {
        //given
        final int a = 43;
        final int b = 12;
        //when
        //then
        assertThat(objectUnderTest.calculate(a, b))
                .isEqualTo(31);
    }

}