package pl.szymon.calculator;

public class Addition implements Operator {

    public int calculate(int a, int b) {
        return a + b;
    }
}
