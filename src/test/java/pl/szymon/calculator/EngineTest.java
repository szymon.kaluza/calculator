package pl.szymon.calculator;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class EngineTest {

    Engine objectUnderTest;

    @Before
    public void before() {
        objectUnderTest = new Engine();
    }

    private class Operator implements pl.szymon.calculator.Operator {
        public int calculate(int a, int b) {
            return 3;
        }
    }

    @Test
    public void shouldSetNumberIfIntegerAIsNull() throws Exception {
        //given
        objectUnderTest.setA(null);
        //when
        objectUnderTest.setNumber(5);
        //then
        assertThat(objectUnderTest.getA()).isEqualTo(5);
    }

    @Test
    public void shouldSetNumberIfIntegerAIsNotNullAndOperatorIsNull() {
        //given
        objectUnderTest.setA(3);
        //when
        objectUnderTest.setNumber(5);
        //then
        assertThat(objectUnderTest.getA()).isEqualTo(35);
    }

    @Test
    public void shouldSetIntegerBIfOperatorIsNotNull() {
        //given
        objectUnderTest.setA(3);
        objectUnderTest.setOperator(new Operator());
        //when
        objectUnderTest.setNumber(5);
        //then
        assertThat(objectUnderTest.getB()).isEqualTo(5);
    }
}