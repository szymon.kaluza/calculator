package pl.szymon.calculator.gui;

import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import pl.szymon.calculator.IntButton;
import pl.szymon.calculator.Operator;
import pl.szymon.calculator.OperatorFactory;
import pl.szymon.calculator.Engine;

public class Calculator {

    @FXML
    Label output;

    private StringBuilder outputContent = new StringBuilder();
    Engine engine = new Engine();

    public void clickNumber(MouseEvent event) {
        final String numberText = getText(event);
        IntButton intButton = new IntButton(Integer.parseInt(numberText));
        engine.setNumber(intButton.getButton());
        updateOutput(numberText);
    }

    public void clickOp(MouseEvent event) {
        final String opText = getText(event);
        char operatorSign = opText.charAt(0);
        Operator operator = OperatorFactory.getOpButton(operatorSign);
        if (!engine.isOperatorSet()) {
            updateOutput(opText);
        }
        engine.setOperator(operator);
        if (operatorSign == 'C') {
            clear();
        }

    }

    public void evaluate() {
        if (engine.hasValidInput()) {
            output.setText(Integer.toString(engine.evaluate()));
        } else {
            output.setText("Błąd");
        }
        engine.clearState();
    }

    private String getText(final MouseEvent mouseEvent) {
        final EventTarget target = mouseEvent.getTarget();
        if (target instanceof Button) {
            return ((Button) target).getText();
        } else if (target instanceof Text) {
            return ((Text) target).getText();

        }
        throw new IllegalArgumentException("Couldnt handle this event target" + mouseEvent.getTarget());
    }

    public void clear() {
        outputContent.delete(0, outputContent.length());
        output.setText(outputContent.toString());
        engine.clearState();
    }

    private void updateOutput(final Object what) {
        outputContent.append(what.toString());
        outputContent.append(" ");
        output.setText(outputContent.toString());
    }

}
