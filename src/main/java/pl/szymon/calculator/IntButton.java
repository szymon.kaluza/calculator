package pl.szymon.calculator;

public class IntButton implements Button {

    private final int button;

    public IntButton(int button) {
        this.button = button;
    }

    public int getButton() {
        return button;
    }

}
