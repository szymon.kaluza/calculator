package pl.szymon.calculator;

import org.assertj.core.api.ThrowableAssert;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class DivisionTest {

    private Division objectUnderTest = new Division();

    @Test
    public void shouldCalculateReturnQuotient() throws Exception {
        //given
        final int a = 43;
        final int b = 12;
        //when
        int result = objectUnderTest.calculate(a, b);
        //then
        assertThat(result).isEqualTo(3);
    }

    @Test
    public void shouldBeInvalidForDivisor0() throws Exception {
        //given
        final int a = 43;
        final int b = 0;
        //when
        boolean result = objectUnderTest.hasValidInput(a, b);
        //then
        assertThat(result).isFalse();

    }




}