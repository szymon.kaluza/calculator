package pl.szymon.calculator;

public interface Operator {

    int calculate(int a, int b);

    default boolean hasValidInput(Integer a, Integer b){
     return true;
    }
}
